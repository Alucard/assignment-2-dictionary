.PHONY: build clear rebuild

build: make_o
	ld -o main main.o lib.o dict.o

make_o:
	nasm -g -f elf64 -o main.o main.asm
	nasm -g -f elf64 -o lib.o lib.asm
	nasm -g -f elf64 -o dict.o dict.asm
clear:
	rm -rf *.o
	rm -rf main

rebuild: 
	clear rebuild

test: 
	python test.py
