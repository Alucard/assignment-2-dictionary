%include "lib.inc"

section .text
global find_word
find_word:
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
	.loop:
		lea rdi, [r13 + 8]
		mov rsi, r12
		call string_equals
		test rax, rax
		jne .success
		mov r13, [r13]
		test r13, r13
		je .fail
		jmp .loop
		.success:
			mov rax, r13
			jmp .out
		.fail:
			xor rax, rax
			jmp .out
	.out:
		pop r13
		pop r12
		ret