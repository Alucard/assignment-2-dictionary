global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global read_string

section .text

; ��������� ��� �������� � ��������� ������� �������
exit: 
    mov rax, 60
    syscall

; ��������� ��������� �� ����-��������������� ������, ���������� � �����
string_length:
    xor rax, rax
    .loop:  
        cmp byte[rdi + rax], 0x0
        je .out
        inc rax
        jne .loop
        .out: 
            ret

; ��������� ��������� �� ����-��������������� ������, ������� � � stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1 
    mov rdi, 1
    syscall
    ret



; ��������� ��� ������� � ������� ��� � stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; ��������� ������ (������� ������ � ����� 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; ������� ����������� 8-�������� ����� � ���������� ������� 
; �����: �������� ����� � ����� � ������� ��� ���������� �������
; �� �������� ��������� ����� � �� ASCII ����.
print_uint:
    mov rsi, rsp
    sub rsp, 32
    mov rax, rdi
    mov r8, 10
    dec rsi
    mov byte[rsi], 0
    xor rdx, rdx
    .loop:
        dec rsi
        div r8
        add dl, `0`
        mov byte[rsi], dl
        xor rdx, rdx
        test rax, rax
        jne .loop
    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; ������� �������� 8-�������� ����� � ���������� ������� 
print_int:
    cmp rdi, 0x0
    jge .pozitive
    neg rdi
    push rdi
    mov rdi, `-`
    call print_char
    pop rdi
    .pozitive:
        call print_uint
        ret

; ��������� ��� ��������� �� ����-��������������� ������, ���������� 1 ���� ��� �����, 0 �����
string_equals:
    .loop:
        mov dl, byte[rdi]
        cmp dl, byte[rsi]
        jnz .error
        inc rdi
        inc rsi
        cmp byte[rdi-1], 0
        jnz .loop
        mov rax, 1
        ret
        .error:
        xor rax, rax
        ret

; ������ ���� ������ �� stdin � ���������� ���. ���������� 0 ���� ��������� ����� ������
read_char:
    xor rax, rax
    push rax
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .out
    cmp rax, -1
    jz .out
    pop rax
    ret
    .out:
        pop r8
        ret 

; ���������: ����� ������ ������, ������ ������
; ������ � ����� ����� �� stdin, ��������� ���������� ������� � ������, .
; ���������� ������� ��� ������ 0x20, ��������� 0x9 � ������� ������ 0xA.
; ��������������� � ���������� 0 ���� ����� ������� ������� ��� ������
; ��� ������ ���������� ����� ������ � rax, ����� ����� � rdx.
; ��� ������� ���������� 0 � rax
; ��� ������� ������ ���������� � ����� ����-����������
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    mov r14, rcx
    xor r14, r14
    .next:
        call read_char
        cmp rax, ` `
        jz .next
        cmp rax, `\t`
        jz .next
        cmp rax, `\n`
        jz .next
        jmp .no_spaces


    .loop:
        call read_char
        cmp rax, ` `
        jz .success
        cmp rax, `\t`
        jz .success
        cmp rax, `\n`
        jz .success
        .no_spaces:
            test rax, rax
            jz .success
            mov byte[r12+r14], al
            inc r14
            cmp r14, r13
            jna .loop
    pop r14
    pop r13
    pop r12
    xor rax, rax
    ret

    .success:
        test r14, r14
        jz .out
        mov byte[r12+r14], 0
        .out:
            mov rax, r12
            mov rdx, r14
            pop r14
            pop r13
            pop r12
            ret


; ��������� ��������� �� ������, ��������
; ��������� �� � ������ ����������� �����.
; ���������� � rax: �����, rdx : ��� ����� � ��������
; rdx = 0 ���� ����� ��������� �� �������
parse_uint:
    push rbx
    xor rax, rax
    xor rdx, rdx
    .loop:
        mov bl, byte[rdi+rdx]
        cmp bl, `0`
        jb .out
        cmp bl, `9`
        ja .out

        mov r8, 10
        push rdx
        mul r8
        pop rdx

        sub bl, `0`
        add al, bl
        inc rdx
        jmp .loop
    .out:
        test rdx, rdx
        jnz .not_zero
        mov rdx, 1
        .not_zero:  
            pop rbx
            ret


; ��������� ��������� �� ������, ��������
; ��������� �� � ������ �������� �����.
; ���� ���� ����, ������� ����� ��� � ������ �� ���������.
; ���������� � rax: �����, rdx : ��� ����� � �������� (������� ����, ���� �� ���) 
; rdx = 0 ���� ����� ��������� �� �������
parse_int:
    cmp byte[rdi], `-`
    jnz .positive
    cmp byte[rdi], `+`
    jz .positive_with_plus
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .zero_length
    neg rax
    inc rdx
    ret
    .positive_with_plus:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .zero_length
        inc rdx
        ret
    .positive:
        call parse_uint
        ret
    .zero_length:
        ret


; ��������� ��������� �� ������, ��������� �� ����� � ����� ������
; �������� ������ � �����
; ���������� ����� ������ ���� ��� ��������� � �����, ����� 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnb .too_long
    push rax
    inc rax
    .loop:
        mov cl, [rdi]
        mov [rsi], cl
        inc rsi
        inc rdi
        dec rax
        test rax, rax
        jnz .loop
    pop rax
    ret
    .too_long:
        xor rax, rax
        ret


print_error:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1 
    mov rdi, 2
    syscall
    ret


read_string:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    mov r14, rcx
    xor r14, r14

    .loop:
        call read_char
        cmp rax, `\n`
        jz .success
        .no_spaces:
            test rax, rax
            jz .success
            mov byte[r12+r14], al
            inc r14
            cmp r14, r13
            jna .loop
    pop r14
    pop r13
    pop r12
    xor rax, rax
    ret

    .success:
        test r14, r14
        jz .out
        mov byte[r12+r14], 0
        .out:
            mov rax, r12
            mov rdx, r14
            pop r14
            pop r13
            pop r12
            ret
