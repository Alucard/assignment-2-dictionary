%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define MAX_LENGTH 256
 
section .bss 
	buff: resb MAX_LENGTH

section .rodata
	err_message: db 'key not found'
	
global _start

section .text
_start:
	mov rdi, buff
	mov rsi, MAX_LENGTH
	call read_string
	test rax, rax
	je .error

	mov rdi, rax
	mov rsi, INDEX
	push rdx
	call find_word
	pop rdx
	test rax, rax
	je .error
	lea rdi, [rax + 8 + rdx + 1]		;rax+8+rdx+1 - ����� ��������
	call print_string
	xor rdi, rdi
	jmp exit
	.error:	
		mov rdi, err_message
		call print_error
		xor rdi, rdi
		jmp exit