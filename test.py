# -*- coding: utf-8 -*-
import subprocess

command_make = "make build"
command_run_main = "./main"
err_message = "key not found"
label_list = [["third word", "third word explanation"], ["a", err_message], [" ", err_message], ["first word", "first word explanation"], ["\n", err_message], ["12jfo2jhfo123jofj134    asdfasdf  asdfa a a aa ", err_message], ["1"*255, err_message], ["1"*256, err_message], ["hello"*1000, err_message]]


def test(test_number, input_label, expected_output):
	print("\n... test " + str(test_number) + " ...")
	process = subprocess.Popen([command_run_main], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate(input=input_label)

	print("input:")
	print(input_label)
	print("\nstdout:")
	print(stdout)
	print("\nstderr:")
	print(stderr)
	if stdout == expected_output or stderr == expected_output:
		print("... test " + str(test_number) + " ended successfully ...\n")
	else:
		print("... test " + str(test_number) + " ended incorrectly, expected " + expected_output + " ...\n")


print("... building main ...")
output = subprocess.check_output(command_make, shell=True)
print(output)

for i in range(1, len(label_list) + 1):
	test(i, label_list[i - 1][0], label_list[i - 1][1])